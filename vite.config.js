import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		// This plugin lets absolute imports work without
		// needing to add "/src" to the beginning.
		tsconfigPaths({
			loose: true, // this line is required to make it work with jsx files
		}),
		react(),
	],
})
