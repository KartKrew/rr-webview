import { group } from 'lib';

export function AddonDetails({ server })
{
	const httpSource = (server.http_source ?? '').replace(/\/+$/, ''); // trim trailing slash
	const linkTitle = httpSource
		? `Download from this server's http_source (${httpSource})`
		: "Download unavailable—server has no http_source";

	const addons = group(server.files ?? [], f => {
		const splitName = f.name.split('_', 2);
		const asoc = {
			R: 'Maps',
			B: 'Maps',
			S: 'Maps',
			T: 'Maps',
			C: 'Characters',
			F: 'Characters',
			L: 'Scripts',
		};
		return (
			splitName.length == 2 &&
			splitName[0][0] == 'D' &&
			asoc[Object.keys(asoc).find(k => splitName[0].includes(k))]
		) || 'Misc';
	}, ['Scripts', 'Maps', 'Characters', 'Misc']);

	function Addon({ file }) {
		const props = httpSource ? {
			href: httpSource + '/' + file.name,
		} : {
			className: 'nohttp',
		};
		return (
			<div className="file">
				<a {...props} title={linkTitle}>{file.name}</a>
			</div>
		);
	}

	function Section({ name }) {
		// Omit empty sections
		return (addons[name].length != 0) && <>
			<h3 className="sep">{name}</h3>
			<div className="addons">
				{addons[name].map((f) => <Addon key={f.name} file={f} />)}
			</div>
		</>;
	}

	return <>
		<Section name="Scripts" />
		<Section name="Maps" />
		<Section name="Characters" />
		<Section name="Misc" />
	</>;
}
