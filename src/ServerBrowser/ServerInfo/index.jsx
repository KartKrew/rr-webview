import React from 'react';
import { ColoredText } from 'ServerBrowser/ColoredText';
import { AddonDetails } from 'ServerBrowser/ServerInfo/AddonDetails';
import { group } from 'lib';

function Address({ text }) {
	const [addressCopied, setAddressCopied] = React.useState(false);

	function handleClick() {
		navigator.clipboard.writeText(text);

		// If the timer is already running, cancel it
		if (addressCopied)
			clearTimeout(addressCopied);

		// Show "Copied!" for 1 second, then return to normal
		setAddressCopied(setTimeout(() => setAddressCopied(false), 1000));
	}

	return (
		<span className="ip" onClick={handleClick}>
			{addressCopied !== false ? 'Copied!' : text}
		</span>
	);
}

function Header({ server, contact }) {
	var gametype = server.gametype;

	if (gametype == 'Race')
		gametype += ` (${server.speed})`;

	return (
		<div className="serverheader-neo">
			<h1 className="servername">
				{server.error ? 'Unreachable' : <ColoredText text={server.server_name} />}
			</h1>
			{contact && <div className="servercontact">Contact: {contact}</div>}
			<div className="serverfuckery pad">
				<div className="serverconnect">
					{server.dedicated ? 'Dedicated' : 'Listen'} server<br />
					<Address text={server.address.join(':')} />
				</div>
				<div className="gameinfo">
					{gametype} - {server.num_humans ?? 0} / {server.max_connections ?? 0} players<br />
					Playing on <strong>{server.map_title ?? 'EMPTY LAND ZONE'}</strong>
				</div>
			</div>
		</div>
	);
}

function PlayerList({ server }) {
	if (server.num_humans)
	{
		const players = group(server.players, ({ team }) => team, ['player', 'spectator']);

		return (
			<div className="players">
				{players.player.map((p, i) => <div key={i}>{p.name}</div>)}
				{players.spectator.map((p, i) => <div key={i} className="spectator">{p.name}</div>)}
			</div>
		);
	}
	else
		return <div>There's no one here...jump in and start a game!</div>;
}

function MOTD({ text }) {
	return text && (
		<div className="tagline">
			<h3 className="sep">MOTD</h3>
			<div className="motd">"{text}"</div>
		</div>
	);
}

export function ServerInfo({ server }) {
	const splitContact = (server.contact ?? '').split('||');

	return <>
		<Header server={server} contact={splitContact[0]} />
		<MOTD text={splitContact[1]} />
		<div className="playerstuff">
			<h3 className="sep">Players</h3>
			<PlayerList server={server} />
		</div>
		<AddonDetails server={server} />
	</>;
}
