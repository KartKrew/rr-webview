import React from 'react';
import { repeat } from 'lib';
import { ColoredText } from 'ServerBrowser/ColoredText';
import { ServerInfo } from 'ServerBrowser/ServerInfo';

import mysteryIcon from 'ServerBrowser/images/mystery.png';
import battleIcon from 'ServerBrowser/images/battle.png';
import easySpeedIcon from 'ServerBrowser/images/Easy.png';
import hardSpeedIcon from 'ServerBrowser/images/Hard.png';
import errorIcon from 'ServerBrowser/images/None.png';
import emptySlotIcon from 'ServerBrowser/images/dot.png';
import spectatorIcon from 'ServerBrowser/images/s.png';
import botIcon from 'ServerBrowser/images/robo.png';
import playerIcon from 'ServerBrowser/images/egg.png';

export const ServerListContext = React.createContext();

function CountryFlag({ countryData }) {
	const props =
		countryData ? {
			src: `https://flagcdn.com/w40/${countryData.iso3166.toLowerCase()}.png`,
			alt: countryData.country_name,
		} : {
			src: mysteryIcon,
			alt: 'Unknown',
		};

	return <img className="serverflag" {...props} />;
}

function ModeIcon({ server }) {
	const table = {
		'Race': {
			'Gear 1': {
				title: 'Easy Speed',
				class: 'gamespeed-Easy',
				icon: easySpeedIcon,
			},
			'Gear 2': {
				title: 'Normal Speed',
				class: 'gamespeed-Normal',
				icon: errorIcon,
			},
			'Gear 3': {
				title: 'Hard Speed',
				class: 'gamespeed-Hard',
				icon: hardSpeedIcon,
			},
		},
		'Battle': {
			'Gear 1': {
				title: 'Battle',
				class: 'gamespeed-battle',
				icon: battleIcon,
			},
		},
		'error': {
			title: 'None',
			class: 'gamespeed-Nnone',
			icon: errorIcon,
		}
	};

	const gametype = table[server.gametype];
	const speed = (gametype && gametype[server.speed]) || table['error'];

	return (
		<div className={`modeicon ${speed.class}`}>
			<img title={speed.title} className={speed.class} src={speed.icon} />
		</div>
	);
}

function ServerName({ server }) {
	if (server.error)
		return <>Unreachable ({server.address.join(':')})</>;
	else
		return <ColoredText text={server.server_name} />;
}

function Heads({ server }) {
	if (!server.players)
		return;

	const num_spectators = server.players.reduce((acc, p) => acc + (p.team == 'spectator'), 0);

	return <>
		{repeat(server.max_connections - server.players.length, (i) => <img key={i} src={emptySlotIcon} />)}
		{repeat(num_spectators, (i) => <img key={i} src={spectatorIcon} />)}
		{repeat(server.num_bots, (i) =>
			<img
				key={i}
				style={{
					filter: 'grayscale(.7)',
					opactity: '70%',
				}}
				src={botIcon}
			/>
		)}
		{repeat(server.players.length - server.num_bots - num_spectators, (i) => <img key={i} src={playerIcon} />)}
	</>;
}

// Some React trivia:
// At the top level of the app (in the ServerBrowser
// component), there are searchData state and
// selectedServer states that update in response to user
// input.
// EVERY component under the ServerBrowser component will
// want to re-render when those states change.
// However, the ServerInfo component is the beefiest
// component in the tree and re-rendering it for every
// single server takes upward of 200 ms cumulatively,
// which creates a very noticeable delay.
//
// The solution is to tell ServerInfo to not re-render
// just because the searchData or selectedServer states
// update.
// Those states are actually unrelated and do not have an
// effect on the output of ServerInfo.
// React.memo will make CopyOut (and thus ServerInfo) only
// re-render if the server or renderedDetails props
// change.
// And these props do not ever change since the page is
// first rendered.
const CopyOut = React.memo(function CopyOut({ server, renderedDetails }) {
	function refCb(node) {
		// Store a reference to the rendered CopyOut in
		// renderedDetails.
		// This will be used for server searching.
		if (node)
			renderedDetails[server.id] = node;
	}

	return (
		<td ref={refCb} className="copyout">
			<ServerInfo server={server} />
		</td>
	);
});

function ListServer({ server, hidden }) {
	const ctx = React.useContext(ServerListContext);

	var cls = 'serverentry';

	if (server.num_humans == server.max_connections)
		cls += ' full';
	else if (server.num_humans == 0)
		cls += ' empty';

	if (server.dedicated)
		cls += ' dedicated';
	else
		cls += ' listen';

	const rowStyle = {};

	if (hidden)
		rowStyle.display = 'none';

	return (
		<tr className={cls} onClick={() => ctx.setSelectedServer(server.id)} style={rowStyle}>
			<td>
				<CountryFlag countryData={ctx.countryMapping[server.address[0]]} />
				<ModeIcon server={server} />
				<ServerName server={server} />
			</td>
			<td>
				<Heads server={server} />
			</td>
			<CopyOut server={server} renderedDetails={ctx.renderedDetails} />
		</tr>
	);
}

function Rows() {
	const ctx = React.useContext(ServerListContext);

	if (ctx.servers.length == 0)
	{
		return (
			<tr>
				<th colSpan="2">No public servers are available.</th>
			</tr>
		);
	}
	else
	{
		return ctx.servers.toSorted((a, b) => {
			// Sort unrechable servers to the bottom of the list
			const errorDiff = ('error' in a) - ('error' in b);
			if (errorDiff)
				return errorDiff;

			if (a.num_humans != b.num_humans)
				return b.num_humans - a.num_humans;

			return b.max_connections - a.max_connections;
		}).map(s =>
			<ListServer
				key={s.id}
				server={s}
				hidden={ctx.searchData.reject.includes(s.id)}
			/>
		);
	}
}

export function ServerList() {
	const ctx = React.useContext(ServerListContext);

	const failedStyle = {};

	if (!ctx.searchData.failed)
		failedStyle.display = 'none';

	return (
		<table className="serverlist">
			<tbody>
				<tr>
					<th>Server Name</th>
					<th>Players</th>
				</tr>
				<tr>
					<th colSpan="2" id="failed" style={failedStyle}>
						No public servers match your filter. (Check your spelling?)
					</th>
				</tr>
				<Rows />
			</tbody>
		</table>
	);
}
