function addonFilter(text, details) {
	const term = text.toLowerCase();
	const reject = Object.entries(details).filter(([id, node]) => !node.textContent.toLowerCase().includes(term));
	return {
		reject: reject.map(([id]) => id),
		failed: reject.length && reject.length == Object.keys(details).length,
	};
}

export function Filters({ setSearchData, renderedDetails }) {
	return (
		<div className="filters">
			<input
				type="text"
				id="search"
				onKeyUp={(ev) => setSearchData(addonFilter(ev.currentTarget.value, renderedDetails))}
				placeholder="Filter servers..."
			/>
		</div>
	);
}
