const colors = {
	'^0': 'inherit',
	'^1': '#df00df',
	'^2': '#ffff0f',
	'^3': '#69e046',
	'^4': '#7373ff',
	'^5': '#ff3f3f',
	'^6': '#a7a7a7',
	'^7': '#ff9736',
	'^8': '#55c8ff',
	'^9': '#cf7fcf',
	'^A': '#d7bb43',
	'^B': '#c7e494',
	'^C': '#c4c4e1',
	'^D': '#f3a3a3',
	'^E': '#bf7b4b',
	'^F': '#ffc7a7',
};

export function ColoredText({ text }) {
	// Match every sequence starting from a caret code
	// (inclusive) up to the next caret code (exclusive).
	// Also match any leading text (not starting with
	// a caret code).
	const seq = /(\^[0-9A-F])?(.*?)(?=\^[0-9A-F]|$)/g;

	return [...text.matchAll(seq)].map(([ _, k, text ], i) =>
		<span key={i} style={{ color: colors[k] }}>{text}</span>);
}
