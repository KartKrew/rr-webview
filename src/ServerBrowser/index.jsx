import React from 'react';
import { ServerList, ServerListContext } from 'ServerBrowser/ServerList';
import { Filters } from 'ServerBrowser/ServerList/Filters';
import { ServerInfo } from 'ServerBrowser/ServerInfo';
import { Landing } from 'ServerBrowser/Landing';
import { useJSONData } from 'lib';
import 'ServerBrowser/style.css';

function InfoPane({ listData, selected }) {
	if (selected)
		return <ServerInfo server={selected} />;
	else
		return (
			<div className="landing">
				<Landing listData={listData} />
			</div>
		);
}

export function ServerBrowser() {
	// URL parameters
	const searchParams = new URLSearchParams(location.search);

	// Raw data used for rendering
	const listData = useJSONData(
		'/list.json',
		{ servers: [], loading: true },
		React.useCallback((data) => {
			data.servers.forEach((s) => s.id = s.address.join());
			return data;
		}, [])
	);
	const countryData = useJSONData('/cc.json', { mapping: {} });

	// "Filters" search state
	const [searchData, setSearchData] = React.useState({ reject: [] });

	// The currently selected (clicked) server listing
	const [selectedServer, setSelectedServer] = React.useState(searchParams.get('id'));

	// Array of rendered ServerInfo DOM nodes for every server
	const renderedDetails = React.useRef({});

	// So many variables are relevant to the ServerList, so pass them through a context
	const ctx = {
		servers: listData.servers,
		countryMapping: countryData.mapping,
		searchData: searchData,
		setSelectedServer: setSelectedServer,
		renderedDetails: renderedDetails.current,
	};

	return <>
		<div className="browse">
			<header>
				<h1>Server Browser</h1>
			</header>
			<Filters setSearchData={setSearchData} renderedDetails={ctx.renderedDetails} />
			<ServerListContext.Provider value={ctx}>
				<ServerList />
			</ServerListContext.Provider>
		</div>
		<div id="serverinfo" className="serverinfo">
			<div id="bghack" className="bghack">
				<InfoPane
					listData={listData}
					selected={ctx.servers.find(s => s.id == selectedServer)}
				/>
			</div>
		</div>
	</>;
}
