function timeString(s) {
	const d = new Date(s * 1000);

	function dig2(n) {
		return String(n).padStart(2, '0');
	}

	const ofs = Math.trunc(d.getTimezoneOffset() / 60);

	// 01/30/2024 20:01:49 GMT-6
	return [
		dig2(d.getDay()),
		dig2(d.getMonth()),
		d.getFullYear()
	].join('/') + ' ' + [
		dig2(d.getHours()),
		dig2(d.getMinutes()),
		dig2(d.getSeconds()),
	].join(':') + ' GMT' + (ofs < 0 ? '+' : '-') + ofs;
}

function DynamicHeader({ listData }) {
	if (listData.loading)
	{
		return <h2>Loading...</h2>;
	}
	else
	{
		const bigcount = {
			players: listData.servers.reduce(
				(acc, s) => acc + (s.num_humans ?? 0), 0),
			servers: listData.servers.length,
		};

		return <>
			<h2>{bigcount.players} players on {bigcount.servers} servers</h2>
			<p>Last updated on {timeString(listData.time)}.</p>
		</>;
	}
}

export function Landing({ listData }) {
	return <>
		<DynamicHeader listData={listData} />
		<p>Looking for the <a href="https://ms.kartkrew.org:8860">SRB2Kart</a> server browser?</p>
		<details>
			<summary>Hosting? Read the rules.</summary>
			<ul>
				<li>Don't use a profane, offensive, or disruptive server name—don't use it to advertise, impersonate other
					servers, or as a platform for harassment.</li>
				<li>Don't host unauthorized edits or repacks of community content. If something wasn't marked Reusable in an MB
					release, obtain permission from its creator before modifying it.</li>
				<li>Don't provide "perks" in exchange for commercial support, donations, or other out-of-game actions; gameplay
					should remain competitively fair, and all content should be accessible to all players.</li>
				<li>Don't permit or encourage hateful, harmful, violent or sexual behavior. Your moderation policies are
					otherwise up to your discretion, but no one should feel unsafe on your server.</li>
			</ul>
			<p>This is a set of broad guidelines for your benefit, not an exhaustive list. Please treat other players and the netgame ecosystem with respect.
			</p>
			<p>For most violations, if you have a <code>server_contact</code> set, we'll try to contact you and resolve things. If you have
				no <code>server_contact</code> set, you'll be immediately delisted—no exceptions.</p>
			<p>To report misconduct or appeal an MS restriction, email<br/><strong>aj [at] worldsbe [dot] st</strong><br/>or contact Tyron on the official Kart Krew Discord.</p>
		</details>
		<details>
			<summary>How to set your server's MOTD for this site</summary>
			<p>Use a double pipe (<code>||</code>) to separate your contact information and the MOTD you want displayed.</p>
			<p>Example: <code>Aigis#1234||A server for toasters.</code></p>
			<p>Note that this is NOT an official spec and may change at any time. Don't use this space to do anything that you wouldn't do in your server title or chat—follow the rules. This also applies to any content you link through this method.</p>
			<p>This only works on the webview; for use in-game, setting <code>motd</code> is still recommended.</p>
		</details>
	</>;
}
