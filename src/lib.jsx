import React from 'react';
import ReactDOM from 'react-dom/client';

// Render app component inside of root element.
//
// app: React component
export function installApp(app) {
	ReactDOM.createRoot(document.getElementById('root'))
		.render(<React.StrictMode>{app}</React.StrictMode>);
}

// Form an object that categorizes an array.
//
// itemsArray: a flat array of elements
// keyFunc: a function that is called on each element and
//          returns a key name
// keys: array of valid keys -- the object will be
//       initialized with these keys
//
// Example:
// group([1, 10, 2, 30], n => n < 10 ? 'ones' : 'tens', ['ones', 'tens', 'hundreds'])
// {'ones': [1, 2], 'tens': [10, 30], 'hundreds': []}
export function group(itemsArray, keyFunc, keys) {
	const obj = Object.fromEntries(keys.map((k) => [k, []]));
	(itemsArray ?? []).forEach(v => obj[keyFunc(v)].push(v));
	return obj;
}

// Form an array by specifying a number of elements and an
// input function.
//
// n: number of elements -- if not a positive number,
//    defaults to 0
// f: function used to create each elements, the index of
//    the element is passed to the function
export function repeat(n, f) {
	return Array.from({ length: Math.max(0, n) }, (_, i) => f(i));
}

// Download JSON data on page load
//
// path: URI to the JSON file
// def: default value for the data -- undefined by default
// cb: function to transform the downloaded data before returning it -- no-op by default
export function useJSONData(path, def, cb) {
	const [data, setData] = React.useState(def);

	React.useEffect(() => {
		fetch(path)
			.then(response => response.json())
			.then(responseData => setData(cb ? cb(responseData) : responseData));
	}, [path, cb]);

	return data;
}
