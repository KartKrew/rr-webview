# Ring Racers Web View NT

This is a React + Vite project. There is also some Python backend.

## 1. Quick start

### 1.1 Install Python backend

1. Make sure Python (at least Python 3.8), pip and jq are installed
  - jq is required for the sync command, used later
2. Download [rrserver](https://git.do.srb2.org/KartKrew/python-rrserver.git)
3. Run `pip3 install <path/to/rrserver>` to install rrserver
  - Note: you may need to set up a virtual environment first

### 1.2 Install React frontend

1. Make sure at least Node.js 18 is installed
2. Run `npm ci` to install website dependencies

### 1.3 Build site

1. Run `npm run build`
  - This will create a directory called `dist`, which can be copied to a web server
2. Run `sh sync.sh <path/to/dist>` to cache master server list and country flags
  - Run this command periodically to recache the lists
  - This command outputs a `list.json` and `cc.json` in the target directory

If you want to test locally before deployment,
run the sync command on the `dist` directory in the project root and then run `npm run preview`
