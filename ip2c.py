#!/usr/bin/env python3

from concurrent.futures import ThreadPoolExecutor, as_completed
from time import time
from urllib.request import urlopen
import argparse
import ipaddress
import json
import sys
import traceback

time_start = time()

def warn(s):
	print(s, file=sys.stderr)

parser = argparse.ArgumentParser()
parser.add_argument(
	'-r',
	help = 'reference file, output from previous run',
	metavar = 'FILE',
)
parser.add_argument(
	'--ttl',
	default = 90000,
	help = 'seconds until old entries in the reference file will be replaced (default: 90000, 25 hrs)',
	metavar = 'SECONDS',
	type = int,
)
parser.add_argument(
	'-t',
	default = 5,
	help = 'number of threads for HTTP requests (default: 5)',
	metavar = 'THREADS',
	type = int,
)
parser.add_argument(
	'-o',
	default = '-',
	help = 'output file (default: "-", stdout)',
	metavar = 'FILE',
)
args = parser.parse_args()

data = {
	'mapping': {},
}

if args.r:
	try:
		with open(args.r) as f:
			d = json.load(f)

		if not isinstance(d, dict) \
		or not isinstance(d.get('mapping'), dict):
			raise ValueError('Reference file does not have the correct layout')

		data = d
	except Exception as e:
		warn(e)
		warn(f"Reference file '{args.r}' malformed, ignoring")

pool = {}

def worker(url):
	with urlopen(url) as r:
		return r.read().decode()

with ThreadPoolExecutor(max_workers=args.t) as executor:
	while True:
		line = sys.stdin.readline().rstrip('\n')
		if line == '':
			break

		try:
			addr = ipaddress.IPv4Address(line)
		except ipaddress.AddressValueError:
			warn(f"Malformed address '{line}' rejected")
			continue

		if addr.is_private:
			warn(f'Private address {addr} rejected')
			continue

		k = str(addr)
		v = data['mapping'].get(k)

		# Reference is too young
		if v and max(v.get('time', 0), time_start) - time_start < args.ttl:
			continue

		if k in pool:
			warn(f'Duplicate ignored {k}')
			continue

		# https://about.ip2c.org/#inputs
		url = f'''http://ip2c.org/?dec={int.from_bytes(addr.packed, byteorder='big')}'''
		f = executor.submit(worker, url)
		pool[k] = (f, url)

	if sys.stderr.isatty():
		n = 0
		for f in as_completed(f for f, _ in pool.values()):
			if f.exception() is None:
				n += 1
				print(f'{n}/{len(pool)}', end='\r', file=sys.stderr)

time_end = time()

for k, (f, url) in pool.items():
	try:
		# https://about.ip2c.org/#outputs
		t = f.result().split(';')
		if t[0] == '1':
			data['mapping'][k] = {
				'country_name': t[3],
				'iso3166': t[1],
				'time': int(time_end),
			}
	except Exception as e:
		traceback.print_exception(e)
		warn(f'{k} ({url}) ran into an error: {e}')

if args.o == '-':
	json.dump(data, sys.stdout, indent=4)
	print()
else:
	with open(args.o, 'w') as f:
		json.dump(data, f, separators=(',', ':'))
