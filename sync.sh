#!/bin/sh
if [ $# -ne 1 ]; then
	>&2 echo "$0 <output directory>"
	exit 1
fi
set -e
lib="$(dirname "$0")"
tmp="$(mktemp -d)"
trap 'rm -rf "$tmp"' EXIT
[ -e "$1/cc.json" ] && cp "$1/cc.json" "$tmp/cc.json"
python3 -m rrserver.tool \
	--app rrserver.packets.RingRacers \
	--packet-timeout 0.7 \
	--server-timeout 60 \
	--ms-api rrserver.masterserver.kartkrew.v22 \
	--ms-address https://ms.kartkrew.org/ms/api/ \
	--ms-servers-get /games/ringracers/{modversion}/servers \
	--ms-version-get /games/ringracers/version \
	--holepunch-server relay.kartkrew.org 7777 \
	-o "$tmp/list.json"
jq -r '.servers[].address[0]' < "$tmp/list.json" |
	python3 -m ip2c \
	-r "$tmp/cc.json" \
	-o "$tmp/cc.json"
cp -t "$1" "$tmp/list.json" "$tmp/cc.json"
